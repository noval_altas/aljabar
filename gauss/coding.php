<!DOCTYPE html>
<html>
<head>
	<title>Gauss Jordan</title>
</head>
<body>
<table>
	<tr>
		<td>
			<?php
			$a = 1;
			$b = -1;
			$c = -1;
			$d = 0;
			$e = 6;
			$f = 2;
			$g = 0;
			$h = 12;
			$i = 6;
			$j = 0;
			$k = 3;
			$l = 8;
			?>
			Soal =
			<table>
				<tr>
					<td>X<small>1</small> -</td>
					<td>X<small>2</small> -</td>
					<td>X<small>3</small></td>
					<td>= 0</td>
				</tr>
				<tr>
					<td>6X<small>1</small> +</td>
					<td>2X<small>2</small></td>
					<td> </td>
					<td>= 12</td>
				</tr>
				<tr>
					<td>6X<small>1</small> </td>
					<td>+</td>
					<td>3X<small>3</small></td>
					<td>= 8</td>
				</tr>
			</table>

			<br>
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td><center>| <?= $a ?></center></td>
								<td><center><?= $b ?></center></td>
								<td><center><?= $c ?></center></td>
								<td><center><?= $d ?> |</center></td>
							</tr>
							<tr>
								<td><center>| <?= $e ?></center></td>
								<td><center><?= $f ?></center></td>
								<td><center><?= $g ?></center></td>
								<td><center><?= $h ?> |</center></td>
							</tr>
							<tr>
								<td><center>| <?= $i ?></center></td>
								<td><center><?= $j ?></center></td>
								<td><center><?= $k ?></center></td>
								<td><center><?= $l ?> |</center></td>
							</tr>
						</table>
					</td>
					<td>
						B<small>2</small> - 6B<small>1</small>
					</td>
				</tr>
			</table>

			<?php
			$E = $e - (6*$a);
			$F = $f - (6*$b);
			$G = $g - (6*$c);
			$H = $h - (6*$d);
			?>
			<br>
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td><center>| <?= $a ?></center></td>
								<td><center><?= $b ?></center></td>
								<td><center><?= $c ?></center></td>
								<td><center><?= $d ?> |</center></td>
							</tr>
							<tr>
								<td><center>| <?= $E ?></center></td>
								<td><center><?= $F ?></center></td>
								<td><center><?= $G ?></center></td>
								<td><center><?= $H ?> |</center></td>
							</tr>
							<tr>
								<td><center>| <?= $i ?></center></td>
								<td><center><?= $j ?></center></td>
								<td><center><?= $k ?></center></td>
								<td><center><?= $l ?> |</center></td>
							</tr>
						</table>
					</td>
					<td>
						B<small>3</small> - 6B<small>1</small>
					</td>
				</tr>
			</table>

			<?php
			$I = $i - (6*$a);
			$J = $j - (6*$b);
			$K = $k - (6*$c);
			$L = $l - (6*$d);
			?>
			<br>
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td><center>| <?= $a ?></center></td>
								<td><center><?= $b ?></center></td>
								<td><center><?= $c ?></center></td>
								<td><center><?= $d ?> |</center></td>
							</tr>
							<tr>
								<td><center>| <?= $E ?></center></td>
								<td><center><?= $F ?></center></td>
								<td><center><?= $G ?></center></td>
								<td><center><?= $H ?> |</center></td>
							</tr>
							<tr>
								<td><center>| <?= $I ?></center></td>
								<td><center><?= $J ?></center></td>
								<td><center><?= $K ?></center></td>
								<td><center><?= $L ?> |</center></td>
							</tr>
						</table>
					</td>
					<td>
						B<small>3</small> - <?= (3/4) ?>B<small>2</small>
					</td>
				</tr>
			</table>

			<?php
			$II = $I - ((3/4)*$E);
			$JJ = $J - ((3/4)*$F);
			$KK = $K - ((3/4)*$G);
			$LL = $L - ((3/4)*$H);
			?>
			<br>
			<table>
				<tr>
					<td>
						<table>
							<tr>
								<td><center>| <?= $a ?></center></td>
								<td><center><?= $b ?></center></td>
								<td><center><?= $c ?></center></td>
								<td><center><?= $d ?> |</center></td>
							</tr>
							<tr>
								<td><center>| <?= $E ?></center></td>
								<td><center><?= $F ?></center></td>
								<td><center><?= $G ?></center></td>
								<td><center><?= $H ?> |</center></td>
							</tr>
							<tr>
								<td><center>| <?= $II ?></center></td>
								<td><center><?= $JJ ?></center></td>
								<td><center><?= $KK ?></center></td>
								<td><center><?= $LL ?> |</center></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
		</td>
		<td>
			<table>
				<tr>
					<td><?= $KK ?> X<small>3</small> = <?= $LL ?></td>
				</tr>
				<tr>
					<td>X<small>3</small> = (<?= $LL/$KK ?>)</td>
				</tr>
			</table>
			<br>
			<table>
				<tr>
					<td><?= $F ?>X<small>2</small> + <?= $G ?> (<?= $LL/$KK ?>) = <?= $H ?></td>
				</tr>
				<tr>
					<td><?= $F ?>X<small>2</small> + (<?= $G*($LL/$KK) ?>) = <?= $H ?></td>
				</tr>
				<tr>
					<td><?= $F ?>X<small>2</small> = <?= $H ?> + <?= ($G*($LL/$KK))*(-1) ?></td>
				</tr>
				<tr>
					<td><?= $F ?>X<small>2</small> = <?= $H+($G*($LL/$KK))*(-1) ?></td>
				</tr>
				<tr>
					<td>X<small>2</small> = <?= ($H+($G*($LL/$KK))*(-1))/$F ?></td>
				</tr>
			</table>
			<br>
			<table>
				<tr>
					<td>X<small>1</small> - <?= ($H+($G*($LL/$KK))*(-1))/$F ?> - (<?= $LL/$KK ?>) = <?= $d ?></td>
				</tr>
				<tr>
					<td>X<small>1</small> - <?= ($H+($G*($LL/$KK))*(-1))/$F ?> + <?= ($LL/$KK)*(-1) ?> = <?= $d ?></td>
				</tr>
				<tr>
					<td>X<small>1</small> = <?= ($H+($G*($LL/$KK))*(-1))/$F ?> - <?= ($LL/$KK)*(-1) ?> = <?= (($H+($G*($LL/$KK))*(-1))/$F)-(($LL/$KK)*(-1)) ?></td>
				</tr>
				<tr>
					<td>X<small>1</small> = <?= (($H+($G*($LL/$KK))*(-1))/$F)-(($LL/$KK)*(-1)) ?></td>
				</tr>
			</table>
		</td>
	</tr>
</table>


</body>
</html>